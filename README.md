# Trainbrains Studio

The application is available for Windows and Linux. Instalation files can de downloadid from the 'releases' catalog.


# Windows

- [ ] Trainbrains Studio.exe (64-bit)
- [ ] Trainbrains Studio.msi (64-bit)

## Installation
- [ ] Download the ZIP file using the link above.
- [ ] Open the Downloads folder , locate the Trainbrains Studio___.zip file, and then extract the installation file (* .exe or * .msi file )
- [ ] Run the application installer by double-clicking on the extracted file.
- [ ] Complete the installation process in the selected directory on the disk.
- [ ] After completing the installation, the program will be available, e.g. through the Start menu

# Linux

- [ ] Trainbrains Studio.deb (64-bit)

## Installation
- [ ] Download the ZIP file using the link above.
- [ ] Open the Downloads folder , locate the Trainbrains Studio___.zip file , and then extract the installation file (* .deb file )
- [ ] Run the application installer by double-clicking on the extracted file.
- [ ] Complete the installation process in the selected directory on the disk.
- [ ] After completing the installation, the program will be available, e.g. through the application menu